#!/usr/bin/python

#
# Project Librarian: Brandon Piotrzkowski
#              Staff Scientist
#              UW-Milwaukee Department of Physics
#              Center for Gravitation & Cosmology
#              <brandon.piotrzkowski@ligo.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import pkg_resources
from setuptools import setup, find_packages


def get_requirements(filename):
    with open(filename, 'r') as f:
        return [str(r) for r in pkg_resources.parse_requirements(f)]


setup(
    name='ligo-raven',
    version='3.3',
    url='http://gracedb.ligo.org',
    author='Alex Urban',
    author_email='alexander.urban@ligo.org',
    maintainer="Brandon Piotrzkowski",
    maintainer_email="brandon.piotrzkowski@ligo.org",
    description='Low-latency coincidence search between external triggers and GW candidates',
    license='GNU General Public License Version 3',
    classifiers=(
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Operating System :: OS Independent",
        "Topic :: Internet",
        "Topic :: Scientific/Engineering :: Astronomy",
        "Topic :: Scientific/Engineering :: Physics"
    ),
    packages=find_packages(),
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    namespace_packages=['ligo'],
    scripts=[
        'bin/raven_query',
        'bin/raven_search',
        'bin/raven_skymap_overlap',
        'bin/raven_coinc_far',
        'bin/raven_calc_signif_gracedb',
        'bin/raven_offline_search'
    ],
    install_requires=get_requirements('requirements.txt'),
    python_requires='>=3.9',
)
