Joint FAR calculation
=====================

One of the main methods to distinguish between a real or noise triggers of any
candidate is to calculate the False Alarm Rate (FAR). The main motivation behind
using the joint FAR between gravitational wave (GW)
and various external triggers, including electromagnetic (EM) bursts and neutrino bursts,
is to indicate which candidate should receive additional follow-up. 

Given the information about the gravitational wave candidate and its
associated astrophysical counterpart, the coincident false alarm rate
:math:`FAR_{c}` is calculated depending on the search method.
Two search methods are:

Untargeted search method
------------------------

In the untargeted search method, we look for independent gravitational wave and external (ext) candidates.
EM candidates of higher statistical significance are considered and GWs can be of very low significance.
So, for the untargeted search analysis, the joint coincident FAR is

.. math::
   FAR_{c} = FAR_{gw} \frac{R_{ext} \Delta t}{I_{\Omega}}

where :math:`FAR_{gw}` is the false alarm rate of the GW candidate given by one of the
GW pipelines such as GstLAL, MBTAOnline, PyCBC Live, SPIIR, or cWB.
:math:`R_{ext}` are the
rate of the unique candidates given by all participating external signal
detectors, :math:`\Delta t` is the coincident time window of the GW-EXT signal, and
:math:`I_{\Omega}` is the sky map overlap integral outlined below.


Targeted search method
----------------------
In the targeted search method, we respond to external triggers found near the times of low-signifiance gravitational wave events.
Because of this, sub-threshold external candidates should now dominate the rate compared to real detections.
This means the joint coincident FAR for this search method is calculated as

.. math::
   FAR_c = \frac{Z}{I_{\Omega}} \left( 1 + \ln(\frac{Z_{max}}{Z}) \right)

where Z is the joint ranking statistic defined as:

.. math::
   Z = FAR_{gw} FAR_{ext} \Delta t

and the max Z value is calculated using the maximum GW and external FAR thresholds.

Skymap overlap integral
-----------------------

In RAVEN, we have built functionality to use different skymap formats for the
skymap overlap integral as first derived in `Ashton et. al. (2018) <https://iopscience.iop.org/article/10.3847/1538-4357/aabfd2/pdf>`_
:

.. math::
   I_{\Omega}(x_{gw}, x_{ext}) =  \int \frac{p({\Omega}| x_{gw}, \mathcal{H}_{gw}^s)
    p({\Omega}| x_{ext}, \mathcal{H}_{ext}^s)}{p({\Omega}| \mathcal{H}^s)} d{\Omega}

Two methods to incorporate the skymap in calculating the skymap overlap integral
(:math:`I_{\Omega}`) are listed as follows:

Hierarchical Equal Area isoLatitude Pixelization (HEALPix) method
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In this method, the sky map overlap integral is calculated as

.. math::
   I_{\Omega}(x_{gw}, x_{ext}) =   N_{pix} \sum_{i=0}^{N_{pix}} P(i| x_{gw}, \mathcal{H}_{gw}^s)
   \cdot P(i| x_{ext}, \mathcal{H}_{ext}^s)

where :math:`N_{pix}` is the number of pixels in a skymap.

Multi-Ordered Coverage (MOC) method
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In this method, we use RA and DEC of given pixel :math:`\Omega_{i}` to match
pixels of MOC skymaps and calculate the overlap integral as

.. math::
   I_{\Omega}(x_{1}, x_{2}) =   4 \pi \sum_{i_{1}}^{N_{pix}} p(\Omega_{1}(i_{1})| x_{1}, \mathcal{H}^s)
   \cdot p(\Omega_{1}(i_{1})| x_{2}, \mathcal{H}^s) \cdot \Delta A (i_{1})

where :math:`p(\Omega_{1}(i)| x_{a}, \mathcal{H}^s)` is the normalized probability density
such that  :math:`\sum_{i}p(\Omega_{1}(i)| x_{a}, \mathcal{H}^s)\Delta A = 1.`

The UNIQ ordering system has an advantage over the standard HEALPix system where equal-area pixels are used.
MOC skymaps can increase resolution around the higher probability regions and vice versa, reducing file size.
Moreover, we find using MOC skymaps gives the same result compared to the HEALPix method with much lower latency.
