.. ligo-raven documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Ligo-raven
==========

Introduction:
-------------

Ligo-raven is a python package that can search for and analyze coincidences between gravitational wave candidates and other external experiments in `GraceDB`_.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart
   examples
   ligo.raven
   bin
   joint_far
   online_search
   offline_search
   contributing
   release

..
  Add offline_search to above when the proper scripts are done. This should include a simple example and how to run the scripts.

.. toctree::
   :maxdepth: 1
   :caption: Git:

   Git repository <https://git.ligo.org/lscsoft/raven>
   changes

.. _`GraceDB`: https://gracedb.ligo.org/
