from astropy.table import Table


class MockGraceDb(object):
    """Mock GraceDB class meant to be similar and callable like
    ligo.gracedb.rest.GraceDb but being populated by a given file, such as
    .csv.

    Parameters
    ----------
    filename : str
        Path to file, such as .csv, to create mock GraceDB class from

    """
    def __init__(self, filename):
        self._service_url = 'https://gracedb-mock.org/api/'
        self.data = Table.read(filename)
        self.mock_gracedb = True

    def events(self, args):
        """Query mock database for external events.

        Parameters
        ----------
        args : str
            String to perform query, in the format of 'group start_time ..
            end_time search pipeline far_cutoff'

        Returns
        -------
        results : list
            List of event dictonaries

        """
        print("Performed search with {}".format(args))
        arg_list = args.split(' ')
        tl, th = float(arg_list[1]), float(arg_list[3])
        search, pipeline, far_eq = arg_list[4], arg_list[5], arg_list[6]
        results = []
        mask = (tl < self.data['gpstime']) * (self.data['gpstime'] < th)
        # Apply additional filters if given
        if search:
            mask *= self.data['search'] == search
        if pipeline:
            mask *= self.data['pipeline'] == pipeline
        if far_eq:
            mask *= (self.data['far'] < float(far_eq.split('<')[1]))
        for result in self.data[mask]:
            results.append({"graceid": result.get('graceid'),
                            "gpstime": result.get('gpstime'),
                            "pipeline": result.get('pipeline'),
                            "search": result.get('search'),
                            "far": result.get('far'),
                            "labels": [],
                            "skymap": result.get('skymap'),
                            "extra_attributes": {
                                "GRB": {
                                    "ra": result.get('ra'),
                                    "dec": result.get('dec'),
                                    "error_radius": result.get(
                                        'error_radius')}}})
        return results

    def superevents(self, args):
        """Query mock database for superevents.

        Parameters
        ----------
        args : str
            String to perform query, in the format of 'start_time .. end_time
            far_cutoff'

        Returns
        -------
        results : list
            List of superevent dictonaries

        """
        print("Performed search with {}".format(args))
        arg_list = args.split(' ')
        tl, th = float(arg_list[0]), float(arg_list[2])
        results = []
        mask = (tl < self.data['t_0']) * (self.data['t_0'] < th)
        # Apply additional filters if given
        try:
            mask *= (self.data['far'] < float(arg_list[3].split('<')[1]))
        except IndexError:
            pass
        for result in self.data[mask]:
            results.append({"superevent_id": result.get('superevent_id'),
                            "t_0": result.get('t_0'),
                            "far": result.get('far'),
                            "labels": [],
                            "skymap": result.get('skymap'),
                            "preferred_event": result.get('preferred_event'),
                            "preferred_event_data":
                            {"group": result.get('group'),
                             "search": result.get('search')}})
        return results
